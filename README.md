Thank you so much for taking time to read this! 

Please do share your thoughts on what I can improve in this project. :)

## Overview

This project is one of the introductory python assignments from Bergen University's
INF109 course. I added the synth part myself.

Overall, the board and the functions is identical to Conwell's Game of Life. 
The only additional thing is that in each iteration, the living cells produce a note.

I thought it could be fun since the user can play around with combinations of sounds, and 
how it develops with the Game of Life dynamics.

graphics.py is the library used for the GUI provided by the INF109 course. The script
for the game is separated into 4 files, namely master_game, game_functions, game_board and synth. 


## Dependencies

This project uses the "synthesizer" library. The installation info can be found [here](https://pypi.org/project/synthesizer/).

The synthesizer library PyAudio to be installed.
It can be easily installed here: ```pip install PyAudio```

Though, I could not install it with pip. If it's the case for you too, the below method might work.

Go to this [link](https://www.lfd.uci.edu/~gohlke/pythonlibs/) and search for "PyAudio". You'll find the .whl
files ready to download. Download the one that suits your machine and python version, got to the directory of where 
it was downloaded, then execute this command: ```pip install <downloaded file name>```.

This was helpful for me: https://stackoverflow.com/questions/52283840/i-cant-install-pyaudio-on-windows-how-to-solve-error-microsoft-visual-c-14

## Usage

In order to run the game, the user must run master_game.py file using the command below.

```python master_game.py```

The game runs with a simple GUI. 

The user must enter the number of iterations they want the game to run for, then press ```Start```.

The ```Clear``` button clears the board (kills all the living cells).

The ```Randomize``` button randomizes the living/dead cells. 

The ```Save``` button saves the current board in a txt file. (this file is deleted when the game ends.)

The ```Load``` button loads the previously saved board. (if a saved file do not exist, the load function won't do anything)


The user can change the living status of a cell by clicking it. 


## Features 
There are two sets of notes that can be used in the chords. 
It can be changed by editing the function call on line 103 in the synth.py file.

```frequecies_seconds()``` will map the notes on the C Major scale in its usual order. This might
have a more chromatic sound. 

```frequencies_third()``` will map the notes major 3rd apart in the c Major scale. This might have
a fuller, more pleasant sound.

## Further Improvements

Once the user presses ```Start```, the game runs until the end of the iterations, and there is no way to stop it
other than closing the window. This could be fixed by adding a ```Pause``` or ```Stop``` button. 
