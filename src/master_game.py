from game_board import *
from game_functions import *
from synth import *


def main():
    """Runs the game"""
    win, field, num_of_iterations, start_butt, clear_butt, randomize_butt, save_butt, \
    load_butt, close_butt = create_game_screen()
    cells, board, win = set_board(win)

    run = True

    while run: 
        mouse_click = win.checkMouse()

        if mouse_click is not None:

            run_this_many_times = int(num_of_iterations.getText())

            if is_inside(start_butt, mouse_click):

                for i in range(run_this_many_times):
                    living_number, living_cells, new_board, changes = iterate(board)
                    if len(living_cells) > 0:
                        play_chord(living_cells)
                    for j in range(len(changes)):
                        x, y = changes[j]
                        update_colors(x, y, new_board, cells)

                    run_this_many_times -= 1
                    if living_number == 0:
                        break
                    board = new_board

            if is_inside(clear_butt, mouse_click):
                board = clear_board(board, cells)

            if is_inside(randomize_butt, mouse_click):
                board = randomize_board(board, cells)

            if is_inside(save_butt, mouse_click):
                save_board(board)

            if is_inside(load_butt, mouse_click):
                if os.path.exists("save_file.txt"):
                    load_board(board, cells)

            if is_inside(field, mouse_click):
                board = is_clicked(mouse_click, board, cells)

            if is_inside(close_butt, mouse_click):
                run = False

    if os.path.exists("save_file.txt"):
        os.remove("save_file.txt")

    win.close()
    print("gg~")


def set_board(win):
    """draws the cells on the board"""

    board = initialize_board()
    cells = initialize_cells()

    for i in range(size()):
        for j in range(size()):
            cells[i][j].draw(win)
            board[i][j] = random_bit()
            update_colors(i, j, board, cells)
    return cells, board, win


main()
