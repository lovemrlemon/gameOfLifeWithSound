from graphics import *


def size():
    """Returns the size of the board."""
    return 7


def initialize_board():
    """Returns a matrix that will store the dead-alive
     values of the cells in the game"""
    board = []

    for x in range(size()):
        sub_board = []
        for y in range(size()):
            sub_board.append(False)
        board.append(sub_board)

    return board


def initialize_cells():
    """Returns a matrix that stores the rectangle
    objects, which will be the cells in the game."""
    cells = []

    for i in range(size()):
        sub_cells = []
        for j in range(size()):
            cell = Rectangle(Point(i, j), Point(i + 1, j + 1))
            sub_cells.append(cell)
        cells.append(sub_cells)

    return cells


def create_game_screen():
    """Creates the game window, and buttons. Returns the window, and the rectangle
    objects of the buttons."""

    win = GraphWin("Game_of_Life", 700, 700)
    win.setBackground(color_rgb(250, 250, 240))
    win.setCoords(-0.2, -1, size() + 0.2, size() + 0.2)

    field = Rectangle(Point(0, 0), Point(size(), size()))
    field.draw(win)

    num_of_iterations = Entry(Point(1.4, -0.5), 4)
    num_of_iterations.setText("0")
    num_of_iterations_text = Text(Point(0.6, -0.5), "Num of iterations: ")
    num_of_iterations_text.draw(win)
    num_of_iterations.draw(win)

    start_butt = Rectangle(Point(1.8, -0.7), Point(2.3, -0.3))
    start_butt.setFill(color_rgb(230, 230, 250))
    start_butt.draw(win)
    start_text = Text(Point(2.05, -0.5), "Start")
    start_text.draw(win)

    clear_butt = Rectangle(Point(2.5, -0.7), Point(3, -0.3))
    clear_butt.setFill(color_rgb(230, 230, 250))
    clear_butt.draw(win)
    clear_text = Text(Point(2.75, -0.5), "Clear")
    clear_text.draw(win)

    randomize_butt = Rectangle(Point(3.17, -0.7), Point(4.03, -0.3))
    randomize_butt.setFill(color_rgb(230, 230, 250))
    randomize_butt.draw(win)
    randomize_text = Text(Point(3.6, -0.5), "Randomize")
    randomize_text.draw(win)

    save_butt = Rectangle(Point(4.2, -0.7), Point(4.7, -0.3))
    save_butt.setFill(color_rgb(230, 230, 250))
    save_butt.draw(win)
    save_text = Text(Point(4.45, -0.5), "Save")
    save_text.draw(win)

    load_butt = Rectangle(Point(4.9, -0.7), Point(5.4, -0.3))
    load_butt.setFill(color_rgb(230, 230, 250))
    load_butt.draw(win)
    load_text = Text(Point(5.15, -0.5), "Load")
    load_text.draw(win)

    close_butt = Rectangle(Point(6.5, -0.7), Point(7, -0.3))
    close_butt.setFill(color_rgb(235, 50, 50))
    close_butt.draw(win)
    close_text = Text(Point(6.75, -0.5), "Close")
    close_text.draw(win)

    return win, field, num_of_iterations, start_butt, clear_butt, randomize_butt, \
        save_butt, load_butt, close_butt
