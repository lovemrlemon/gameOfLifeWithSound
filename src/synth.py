from game_board import size
from synthesizer import Player, Synthesizer, Waveform


def initialize_notes():
    """Creates a matrix the size of the board, where the corresponding
    sounds will be stored later."""
    notes = []
    for i in range(size()):
        sub_notes = []
        for j in range(size()):
            sub_notes.append(0.0)
        notes.append(sub_notes)
    return notes


def frequencies_seconds():
    """Sets the frequencies of each note in C major scale (interval
    of seconds, and stores them in the matrix "notes"."""
    notes = initialize_notes()

    for line in range(1, 8):
        if line == 1:
            notes[0][0] = 293.6  # D4
            notes[size() - 1][size() - 1] = 932.3 # Bb5

        if line == 2:
            for i in range(line):
                notes[i][line - 1 - i] = 329.6  # E4
                notes[size() - line + i][size() - 1 - i] = 880.0  # A5

        if line == 3:
            for i in range(line):
                notes[i][line - 1 - i] = 349.2  # F4
                notes[size() - line + i][size() - 1 - i] = 783.9  # G5

        if line == 4:
            for i in range(line):
                notes[i][line - 1 - i] = 392.0  # G4
                notes[size() - line + i][size() - 1 - i] = 698.4  # F5

        if line == 5:
            for i in range(line):
                notes[i][line - 1 - i] = 440.0  # A4
                notes[size() - line + i][size() - 1 - i] = 659.2  # E5

        if line == 6:
            for i in range(line):
                notes[i][line - 1 - i] = 466.1  # Bb4
                notes[size() - line + i][size() - 1 - i] = 587.3  # D5

        if line == 7:
            for i in range(line):
                notes[i][line - 1 - i] = 523.2  # C4

    return notes


def frequencies_thirds():
    """Sets the frequencies of each note in C major scale (interval
    of thirds, and stores them in the matrix "notes"."""
    notes = initialize_notes()

    for line in range(1, 8):
        if line == 1:
            notes[0][0] = 164.8  # E3
            notes[size() - 1][size() - 1] = 1760.0  # A6

        if line == 2:
            for i in range(line):
                notes[i][line - 1 - i] = 196.0  # G3
                notes[size() - line + i][size() - 1 - i] = 1396.9  # F6

        if line == 3:
            for i in range(line):
                notes[i][line - 1 - i] = 233.0  # Bb3
                notes[size() - line + i][size() - 1 - i] = 1174.6  # D6

        if line == 4:
            for i in range(line):
                notes[i][line - 1 - i] = 293.6  # D4
                notes[size() - line + i][size() - 1 - i] = 932.3  # Bb5

        if line == 5:
            for i in range(line):
                notes[i][line - 1 - i] = 349.2  # F4
                notes[size() - line + i][size() - 1 - i] = 783.9  # G5

        if line == 6:
            for i in range(line):
                notes[i][line - 1 - i] = 440.0  # A4
                notes[size() - line + i][size() - 1 - i] = 659.2 # E5

        if line == 7:
            for i in range(line):
                notes[i][line - 1 - i] = 523.2  # C5
    return notes


def create_chord(living_cells):
    """Creates the chord that will be played for each iteration."""

    notes = frequencies_seconds()
    chord = []
    for i in range(len(living_cells)):
        chord.append(notes[living_cells[i][0]][living_cells[i][1]])
    return chord


def play_chord(living_cells):
    """Plays the chord."""
    player = Player()
    player.open_stream()
    synthesizer = Synthesizer(osc1_waveform=Waveform.sine, osc1_volume=1.0, use_osc2=False)
    chord = create_chord(living_cells)
    player.play_wave(synthesizer.generate_chord(chord, 0.5))
