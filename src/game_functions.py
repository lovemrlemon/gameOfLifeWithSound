from graphics import *
from random import choice
from game_board import size


def living_neighbors(x, y, board):
    """Counts the number of living neighbors of the cell in
    the given coordinate (x, y). If either coordinate is on
    the edge, it wraps: i.e like a sphere. Finally, returns
    the number of living neighbors."""
    neighbors = 0

    left_x = x - 1
    right_x = x + 1
    down_y = y - 1
    up_y = y + 1

    # making sure that we're on a torus <3
    if x - 1 < 0:
        left_x = x - 1 + size()
    if x + 1 > size() - 1:
        right_x = x + 1 - size()
    if y - 1 < 0:
        down_y = y - 1 + size()
    if y + 1 > size() - 1:
        up_y = y + 1 - size()

    # now, counting the neighbors =3
    if board[left_x][down_y]:
        neighbors += 1
    if board[left_x][y]:
        neighbors += 1
    if board[left_x][up_y]:
        neighbors += 1
    if board[x][down_y]:
        neighbors += 1
    if board[x][up_y]:
        neighbors += 1
    if board[right_x][down_y]:
        neighbors += 1
    if board[right_x][y]:
        neighbors += 1
    if board[right_x][up_y]:
        neighbors += 1

    return neighbors


def iterate(board):
    """Does one iteration, returns the next generation of cells.
    Returns (living_number, living_cells, board, changes), where
    living is the number of alive cells, living_cells are the
    coordinates of the living cells, and board is a matrix of boolean
    values corresponding to the liveliness of each cell. Changes is
    a list of coordinates that have changed."""
    new_board = []
    for i in range(size()):
        row = []
        for j in range(size()):
            row.append(False)
        new_board.append(row)

    changes = []
    living_number = 0
    living_cells = []

    for x in range(size()):
        for y in range(size()):
            if board[x][y]:
                if living_neighbors(x, y, board) == 2 or living_neighbors(x, y, board) == 3:
                    new_board[x][y] = True
                    living_number += 1
                    living_cells.append((x, y))
            else:
                if living_neighbors(x, y, board) == 3:
                    new_board[x][y] = True
                    living_number += 1
                    living_cells.append((x, y))

            if board[x][y] != new_board[x][y]:
                changes.append((x, y))

    return living_number, living_cells, new_board, changes


def is_clicked(point, board, cells):
    """When a cell is clicked, it will change color"""
    x_point = point.getX()
    y_point = point.getY()

    if board[int(x_point)][int(y_point)]:
        board[int(x_point)][int(y_point)] = False
    else:
        board[int(x_point)][int(y_point)] = True

    update_colors(int(x_point), int(y_point), board, cells)

    return board


def randomize_board(board, cells):
    """Randomizes the board from scratch."""
    for i in range(size()):
        for j in range(size()):
            board[i][j] = random_bit()
            update_colors(i, j, board, cells)
    return board


def clear_board(board, cells):
    """Clears the board by killing off every cell."""
    for i in range(size()):
        for j in range(size()):
            board[i][j] = False
            update_colors(i, j, board, cells)
    return board


def save_board(board):
    """Saves the values in the matrix "board" in a
    text file"""
    save_file = open("save_file.txt", "w")
    for i in range(size()):
        for j in range(size()):
            if board[i][j]:
                save_file.write("T ")
            else:
                save_file.write("F ")
        save_file.write("\n")
    save_file.close()


def load_board(board, cells):
    """Updates the values in board with the previously
    saved board, and updates the cells."""
    save_file = open("save_file.txt", "r")
    for i in range(size()):
        row = save_file.readline()
        values = row.split(" ")
        for j in range(size()):
            if values[j] == "F":
                board[i][j] = False
            else:
                board[i][j] = True
            update_colors(i, j, board, cells)
    save_file.close()


def is_inside(box, mouse_click):
    """Returns true if the mouse click is inside a
    given box"""
    mouse_x = mouse_click.getX()
    mouse_y = mouse_click.getY()
    point1 = box.getP1()
    point2 = box.getP2()
    point1_x = point1.getX()
    point1_y = point1.getY()
    point2_x = point2.getX()
    point2_y = point2.getY()

    if point1_x <= mouse_x <= point2_x and \
            point1_y <= mouse_y <= point2_y:
        return True
    else:
        return False


def update_colors(x, y, board, cells):
    """Sets the color in cells[x][y] to correct the value according
    to the status of its corresponding index in board."""
    if board[x][y]:
        cells[x][y].setFill(color_rgb(220, 20, 60))
    else:
        cells[x][y].setFill(color_rgb(112, 128, 144))


def random_bit():
    """Returns a random value of True and False"""
    return choice([True, False])
